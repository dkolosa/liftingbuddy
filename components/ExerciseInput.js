import React, {useState} from 'react'
import {View, TextInput, Button, StyleSheet, Modal} from 'react-native'


const ExerciseInput = props => {

  const [enteredExercise, setExercise] = React.useState('');

  const getInput = (enteredText) => {setExercise(enteredText);}

  const addExerciseHandler = () => {
    props.onAddExercise(enteredExercise);
    setExercise('');
  };


    return (
      <Modal visible={props.visible} animationType='slide'>
        <View style={styles.inputContainer}>
          <TextInput 
            placeholder='Input Name' style={styles.inputBox}
            onChangeText={getInput}
            value={enteredExercise}
          />
          <View style={styles.buttons}>
            <Button
              title='Add Exercise'
              color='red'
              style={styles.confirmButton}
              onPress={addExerciseHandler}
            />
            <Button
              title='Cancel'
              style={styles.confirmButton}
              onPress={props.onCancel}
            />
          </View>
        </View>
      </Modal>
    );

};

const styles = StyleSheet.create({
    inputContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    inputBox: {
      borderColor:'#000', 
      padding: 10, 
      width: '40%',
      marginBottom: 10,
    },
    confirmButtonButton: {
      width: '20%'
    },
    buttons: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '30%'
    }

})

export default ExerciseInput;