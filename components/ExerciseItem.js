import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from "react-native"

const ExerciseItem = props => {
    return (
    <TouchableOpacity onPress={props.onDelete.bind(this, props.id)}>
        <View style={styles.item}>
            <Text>{props.title}</Text>
        </View>
    </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
      },

})

export default ExerciseItem;