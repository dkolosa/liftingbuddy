import react from "react";

const CreateUser = props => {
    const initialBirthDay = [
        {id:"day", value: 1},
        {id:"month", value: 1},
        {id: "year", value:1990},
      ]
    
      const date = [
        { id: "day", label: "", min: 0, max: 31 },
        { id: "month", label: "", min: 0, max: 12 },
        { id: "year", label: "", min: 1900, max: new Date().getFullYear()}
      ]

    const [userName, setName] = React.useState("Name");
    const [birthday, setBirthday] = React.useState(initialBirthDay);

}

export default CreateUser;