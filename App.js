import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, FlatList } from 'react-native';

import ExerciseItem from './components/ExerciseItem';
import ExerciseInput from './components/ExerciseInput';

export default function App() {

  const [listExercises, setExercises] = React.useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const addExercisehandler = getTitle => 
  {
    setExercises(listExercises => [...listExercises, 
        {id: Math.random().toString(), value: getTitle} ]
    );
    setIsAddMode(false);
  };

  const removeExerciseHandler = getID => {
    setExercises(listExercises => {
      return listExercises.filter((exercise) => exercise.id !== getID);
    });
  }

  const cancelExerciseHandler = () => {
    setIsAddMode(false);
  }

  return (
    <View style={styles.container}>
        <Button title='Add new Exercise' onPress={() => setIsAddMode(true)}/>
        <ExerciseInput onCancel={cancelExerciseHandler} visible={isAddMode} onAddExercise={addExercisehandler}/>
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={listExercises}
          renderItem={itemData => 
            <ExerciseItem
              id={itemData.item.id} 
              onDelete={removeExerciseHandler}
              title={itemData.item.value}
            />}
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    padding:100,
  },
});
